# Tugas 2

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Detail tugas dan app ini

Buatlah proyek Toko Online dengan ketentuan berikut:

- Memiliki halaman login dengan proses autentikasi sederhana
- Memiliki halaman utama dengan navigasi ke minimal 3 halaman
- Memanfaatkan components, state, lifecycle, penanganan events, fragment

Terdapat kombinasi penggunaan class component tanpa hook dan function
component dengan hooks (`useState` dan `useEffect`)

#### Author

Gottfried CPN - DTS UI ITP FE 02