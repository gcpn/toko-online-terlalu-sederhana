import React from 'react'
import { Redirect } from 'react-router-dom'
import './LoginPage.css'

class LoginPage extends React.Component {
    passwordLength = 6
    username = ""
    password = ""
    constructor(props) {
        super(props)
        this.state = {
            isLoggedIn: false
        }
        this.authenticate = this.authenticate.bind(this)
    }

    // tidak perlu submit karena ini bukan web app konvensional
    // (button ada di dalam tag form)
    handleSubmit(e) {
        e.preventDefault()
    }

    authenticate() {
        // otentikasi sederhana dengan string length
        // semua username dan pass valid jika length > n
        const n = this.passwordLength
        if (this.username.length >= n && this.password.length >= n) {
            this.setState({
                isLoggedIn: true
            })
            console.log('login berhasil')
        }
        // cetak ke console utk tes
        console.log(`username: ${this.username}, password: ${this.password}`)  // tes
    }

    // contoh penggunaan component life cycle
    componentDidMount() {
        // Pura-pura mencoba koneksi ke API
        document.getElementById('status-api').innerHTML += "Terhubung ke server!"
    }

    render() {
        // render login page jika belum login
        // jika login sukses, langsung redirect ke laman Beli
        // contoh ini belum menggunakan session atau semacamnya,
        // sehingga status login hanya selama obyek ini aktif
        let form = <Redirect to="/" />
        if (!this.state.isLoggedIn) {
            form = (
                <form className="form-login" onSubmit={this.handleSubmit}>
                    <h2>Silakan login</h2>
                    <div className="form-group">
                        <label for="username">Username</label>
                        <input onChange={(e) => { this.username = e.target.value }} type="text" className="form-control" id="username" />
                    </div>
                    <div className="form-group">
                        <label for="password">Password</label>
                        <input onChange={(e) => { this.password = e.target.value }} type="password" className="form-control" id="password" />
                    </div>
                    <button onClick={this.authenticate} className="btn btn-primary btn-block mt-3">Login</button>
                    <p><small id="status-api">Status server: </small></p>
                </form>
            )
        }
        return form
    }
}

export default LoginPage